# C# 实时折线图与波形图 Demo

## 简介

本项目演示如何利用Visual Studio中自带的Chart图表控件来创建实时动态展示的折线图和波形图。旨在为开发者提供一个简单易懂的学习案例，适用于需要在C#应用程序中集成实时数据可视化功能的场景。所有内容皆为学习交流之用，欢迎提出宝贵建议与指正。

## 涉及知识点

- **Chart控件**：功能全面，支持绘制柱状图、折线图、波形图、饼状图等多种图形，极大简化图表开发与自定义过程。
- **Chart控件核心概念**：
  - **ChartArea**：图表区域，单一Chart可包含多个ChartArea，可重叠显示。
  - **Series**：数据系列，代表数据线，每个ChartArea内可设置多条Series，直接归属ChartArea管理。
  - **AxisX (X轴) / AxisY (Y轴)**：主坐标轴，每个ChartArea都配备相应的坐标轴系统，支持主辅坐标轴。

- **Queue集合**：基于先进先出（FIFO）原则的集合类，对于处理实时数据流尤为重要。
  - 主要方法包括`Dequeue()`用于移除并返回队列首部的对象，以及`Enqueue()`用于将对象加入队列尾部。

## 核心实现

项目通过模拟数据流，结合Queue集合高效管理数据，实现数据的实时更新显示。重点在于如何正确更新Series的数据点，确保图表能流畅、实时地反映数据变化，非常适合于信号处理、数据分析等实时监控应用。

### 使用指南

1. **环境要求**：确保你的开发环境为Visual Studio，并已安装相关.NET Framework。
2. **打开项目**：导入项目至Visual Studio，配置好相关引用。
3. **运行Demo**：编译并运行，观察如何随着新数据的添加，图表实时变化，展现出动态折线图或波形图的效果。
4. **定制化调整**：可根据实际需求调整Chart的样式、颜色、数据更新频率等属性。

## 注意事项

在实际应用中，根据数据量和刷新率的调整，可能需要优化内存管理和性能调优，以保证程序的稳定性和响应速度。

---

希望这个Demo能够帮助你快速理解和掌握如何在C#应用中利用Chart控件进行实时数据的可视化展现。如果你有任何疑问或改进建议，欢迎贡献代码或在项目页面留言讨论。